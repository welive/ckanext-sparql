# Translations template for ckanext-sparql.
# Copyright (C) 2016 ORGANIZATION
# This file is distributed under the same license as the ckanext-sparql project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ckanext-sparql 0.0.1\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2016-02-11 10:11+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 0.9.6\n"

#: ckanext/sparql/templates/package/read_base.html:6
msgid "SPARQL endpoint"
msgstr ""

#: ckanext/sparql/templates/sparql/sparql_endpoint.html:7
msgid "SPARQL endpoint for "
msgstr ""

#: ckanext/sparql/templates/sparql/sparql_endpoint.html:10
msgid "Query"
msgstr ""

#: ckanext/sparql/templates/sparql/sparql_endpoint.html:17
msgid "Send"
msgstr ""

