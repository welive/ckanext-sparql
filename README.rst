.. You should enable this project on travis-ci.org and coveralls.io to make
   these badges work. The necessary Travis and Coverage config files have been
   generated for you.

.. image:: https://travis-ci.org/memaldi/ckanext-sparql.svg?branch=master
    :target: https://travis-ci.org/memaldi/ckanext-sparql

.. image:: https://coveralls.io/repos/memaldi/ckanext-sparql/badge.png?branch=master
  :target: https://coveralls.io/r/memaldi/ckanext-sparql?branch=master

=============
ckanext-sparql
=============

An extension for integrating RDF into CKAN.

------------
Requirements
------------

* CKAN 2.4.1


------------------------
Development Installation
------------------------

To install ckanext-sparql for development, activate your CKAN virtualenv and
do::

    git clone https://github.com/memaldi/ckanext-sparql.git
    cd ckanext-sparql
    python setup.py develop
    pip install -r dev-requirements.txt


---------------
Config Settings
---------------

In the configuration .ini file of CKAN, include the following configuration variables::

    [plugin:sparql]
    welive_api = <URL to WeLive API>
